<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <title>MAPLE - Muramoto Audio-Visual Philippines</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Sans">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/prettyPhoto/css/prettyPhoto.css">
        <link rel="stylesheet" href="assets/css/flexslider.css">
        <link rel="stylesheet" href="assets/css/font-awesome.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        
        
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="assets/ico/logo.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Header -->
        <div class="container">
            <div class="header row">
                <div class="span12">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </a>
                            <h1>
                                <a class="brand" href="index.phtml">MAPLE - Muramoto Audio-Visual Philippines</a>
                            </h1>
                            <div class="nav-collapse collapse">
                                <ul class="nav pull-right">
                                    <li class="current-page">
                                        <a href="index.phtml"><i class="icon-home"></i><br />Home</a>
                                    </li>
                                    
                                    <li>
                                        <a href="about.phtml"><i class="icon-user"></i><br />About</a>
                                    </li>
                                    <li>
                                        <a href="portfolio.phtml"><i class="icon-camera"></i><br />Systems</a>
                                    </li>
                                    <li>
                                        <a href="links.phtml"><i class="icon-envelope-alt"></i><br />Links</a>
                                    </li>
                                    <li>
                                        <a href="contact.phtml"><i class="icon-envelope-alt"></i><br />Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Slider -->
        <div class="slider">
            <div class="container">
                <div class="row">
                    <div class="span10 offset1">
                        <img src="assets/img/portfolio/work.jpg">
                        
                    </div>
                </div>
            </div>
        </div>

        <!-- Site Description -->
        <div class="presentation container">
            <h2><span class="violet">Muramoto's Corporate Spirit</span></h2>
            <p>Crucial to creating high-quality products are proven technology,
			individual strength of each personnel, and a corporate culture based
			on harmony. This belief is embodied in our primary philosophy,
			"CP&H" Creation, Power and Harmony.</p>
        </div>

        <!-- Services -->
        <div class="what-we-do container">
            <div class="row">
                <div class="service span3">
                    <div class="icon-awesome">
                        <i class="icon-eye-open"></i>
                    </div>
                    <h4>Creation <br /><i>Spirit</i></h4>
                    <p>We open our eyes to the needs of a new society so
					that we may be able to satisfy them as well as to
					satisfy our own need to create.</p>
                </div>
                <div class="service span3">
                    <div class="icon-awesome">
                        <i class="icon-group"></i>
                    </div>
                    <h4>Power <br /><i>of Togetherness</i></h4>
                    <p>We harness each person's power to act and to
					be creative so that together we will continue
					to exist and to grow as a company.</p>
                </div>
                <div class="service span3">
                    <div class="icon-awesome">
                        <i class="icon-globe"></i>
                    </div>
                    <h4>Harmony <br /><i>within the Company</i></h4>
                    <p>We strive to maintain harmony within our
					company so that we may be able to act together
					swiftly and more reliably. </p>
                </div>
            </div>
        </div>

        <!-- Latest Work 
        <div class="portfolio container">
            <div class="portfolio-title">
                <h3>Our Running Systems</h3>
            </div>
            <div class="row">
                <div class="work span3">
                    <img src="assets/img/portfolio/mps.jpg" alt="">
                    <h4>MPS</h4>
                    <p>Maple Production System. . .</p>
                    <div class="icon-awesome">
                        <a href="assets/img/portfolio/mps.jpg" rel="prettyPhoto"><i class="icon-search"></i></a>
                        <a target="_blank" href="http://p1web03/MPS"><i class="icon-link"></i></a>
                    </div>
                </div>
                <div class="work span3">
                    <img src="assets/img/portfolio/tamps.png" alt="">
                    <h4>TAMPS</h4>
                    <p>Time Attendance Monitoring System. . .</p>
                    <div class="icon-awesome">
                        <a href="assets/img/portfolio/tamps.png" rel="prettyPhoto"><i class="icon-search"></i></a>
                        <a target="_blank" href="http://p1web02/TAMPS/"><i class="icon-link"></i></a>
                    </div>
                </div>
                <div class="work span3">
                    <img src="assets/img/portfolio/edb.gif" alt="">
                    <h4>EDB</h4>
                    <p>Employee Database. . .</p>
                    <div class="icon-awesome">
                        <a href="assets/img/portfolio/edb.gif" rel="prettyPhoto"><i class="icon-search"></i></a>
                        <a href="http://p1web02/EDB/"><i class="icon-link"></i></a>
                    </div>
                </div>
                <div class="work span3">
                    <img src="assets/img/portfolio/poms.png" alt="">
                    <h4>POMS</h4>
                    <p>Purchase Order Monitoring System. . .</p>
                    <div class="icon-awesome">
                        <a href="assets/img/portfolio/poms.png" rel="prettyPhoto"><i class="icon-search"></i></a>
                        <a href="http://p1web03/POMS/"><i class="icon-link"></i></a>
                    </div>
                </div>
            </div>
        </div> -->

        <!-- Testimonials -->
        <div class="testimonials container">
            <div class="testimonials-title">
                <h3>***</h3>
            </div>
            <!-- <div class="row">
                <div class="testimonial-list span12">
                    <div class="tabbable tabs-below">
                        <div class="tab-content">
                            <div class="tab-pane active" id="A">
                                <img src="assets/img/testimonials/1.jpg" title="" alt="">
                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Lorem ipsum dolor sit amet, consectetur..."<br /><span class="violet">Lorem Ipsum, dolor.co.uk</span></p>
                            </div>
                            <div class="tab-pane" id="B">
                                <img src="assets/img/testimonials/2.jpg" title="" alt="">
                                <p>"Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat..."<br /><span class="violet">Minim Veniam, nostrud.com</span></p>
                            </div>
                            <div class="tab-pane" id="C">
                                <img src="assets/img/testimonials/3.jpg" title="" alt="">
                                <p>"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Lorem ipsum dolor sit amet, consectetur..."<br /><span class="violet">Lorem Ipsum, dolor.co.uk</span></p>
                            </div>
                            <div class="tab-pane" id="D">
                                <img src="assets/img/testimonials/1.jpg" title="" alt="">
                                <p>"Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat..."<br /><span class="violet">Minim Veniam, nostrud.com</span></p>
                            </div>
                        </div>
                       <ul class="nav nav-tabs">
                           <li class="active"><a href="#A" data-toggle="tab"></a></li>
                           <li class=""><a href="#B" data-toggle="tab"></a></li>
                           <li class=""><a href="#C" data-toggle="tab"></a></li>
                           <li class=""><a href="#D" data-toggle="tab"></a></li>
                       </ul>
                   </div>
                </div>
            </div> -->
        </div> 

        <!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="widget span5">
                        <h4>About Us</h4>
                        <p style="text-align:justify;">Muramoto Audio-Visual Philippines, Incorporated (MAPLE) is one of the manufacturing facilities
						of the Muramoto Group of Companies. Muramoto Group is trusted as the primary source of audio-visual
						mechanisms for automotives and general devices serving major Japanese, American, and European
						companies. Our Group operates in 10 countries in Asia, North America, and Europe employing more
						than 10,000 professionals.</p>
                    </div>
                    <div class="widget span1">
                        <h4>&nbsp;</h4>
                    </div>
                    <div class="widget span5">
                        <h4>Contact Us</h4>
                        <p><i class="icon-map-marker"></i> Address: 4th St. 3rd Ave., Block D-5, Mactan Economic Zone 1 
						Lapu-Lapu City, Cebu Philippines 6015</p>
                        <p><i class="icon-phone"></i> Factory 1 Phone: (032) 340 0296</p>
                        <p><i class="icon-phone"></i> Factory 2 Phone: (032) 340 1580</p>
                        <p><i class="icon-envelope-alt"></i> IS Group Email: <a href="mailto:is@maple.muramoto.com">is@maple.muramoto.com</a></p>
                    </div>
                </div>
                <div class="footer-border"></div>
                <div class="row">
                    <div class="copyright span6">
                        <p>A JOINT IDEA OF ANGEL & TODS<br />Copyright &copy; 2014 IS Group - All Rights Reserved. Template by <a href="http://azmind.com" target="_blank">Azmind</a>.</p>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Javascript -->
        <script src="assets/js/jquery-1.8.2.min.js"></script>
        <script src="assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.flexslider.js"></script>
        <script src="assets/js/jquery.tweet.js"></script>
        <script src="assets/js/jflickrfeed.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="assets/js/jquery.ui.map.min.js"></script>
        <script src="assets/js/jquery.quicksand.js"></script>
        <script src="assets/prettyPhoto/js/jquery.prettyPhoto.js"></script>
        <script src="assets/js/scripts.js"></script>

    </body>

</html>

